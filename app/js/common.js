'use strict';

$(document).ready(function () {

    $(".owl-carousel").owlCarousel({
        margin: 0,
        items: 1
    });

    $(".collection-popup-btn").click(function () {
        let collection = $(this).data("collection");

        $(`.collection-popup .list:not(.${collection})`).hide();
        $(".collection-popup-wrap").fadeIn();
    });

    $("#info").click(function (e) {
        e.preventDefault();
        $(".info-popup-wrap").fadeIn();
    });

    $(".popup").click(function (e) {
        let popup = $(this)
        if ( !e.target.closest(".popup-body") || e.target.closest(".close-popup") ) {
            e.preventDefault();
            popup.hasClass("collection-popup-wrap")
                ? closeCollectionPopup()
                : popup.fadeOut();
        }
    });

    function closeCollectionPopup() {
        $(".collection-popup-wrap").fadeOut(100);
        $(".collection-popup .list").show();
    }

});
